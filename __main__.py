import discord
import random
import os
import toml
import re
from dotenv import load_dotenv

load_dotenv()

with open('zbyszek.toml', 'rb') as f:
    s = toml.loads(f.read().decode('utf-8'))


class Zbyszek(discord.Client):
    async def on_ready(self):
        print(f'Logged as {self.user}')
        await self.change_presence(activity=discord.Game(name='***** ***'))

    async def on_message(self, message):
        if message.author.id == self.user.id:
            return

        mention = re.compile(fr'<@(!*){os.getenv("BOT_ID")}>')
        match = mention.search(message.content)

        if match:
            await message.reply(random.choice(s['sentences']), mention_author=False)


def main():
    client = Zbyszek()
    client.run(os.getenv('TOKEN'))


if __name__ == '__main__':
    main()