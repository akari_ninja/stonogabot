## Stonogabot
[Invite the bot to your server](https://discord.com/api/oauth2/authorize?client_id=804442891308498984&permissions=0&scope=bot)

### How to run:
```sh
$ git clone https://gitlab.com/akari_ninja/stonogabot.git
$ cd stonogabot
$ python3 -m pip install -r requirements.txt (CentOS/RHEL will require installing `multidict` and `yarl` using `yum`)
$ cp .env.example .env
$ nano .env (or whatever, do not forget sudo or root)
$ python3 __main__.py
```

### Stonogabot in action:
![](usage_example.png)

### License:
Stonogabot is licensed under CC0 and is derived from Balcerbot by @sech1p
